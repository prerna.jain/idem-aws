# Find leftover Subnets
orphan_subnets:
  exec.run:
    - path: aws.ec2.subnet.list
    - kwargs:
        name: null
        filters:
          - name: "tag:Name"
            values:
              - "idem-fixture-subnet-*"

# Remove leftover Subnets
#!require:orphan_subnets
{% for resource in hub.idem.arg_bind.resolve('${exec:orphan_subnets}') %}

cleanup-{{ resource['resource_id'] }}:
  aws.ec2.subnet.absent:
    - resource_id: {{ resource['resource_id'] }}
    - require:
        - cleanup-instance-{{ resource['resource_id'] }}

{% endfor %}
#!END
