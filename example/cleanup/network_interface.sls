# Find leftover Network Interfaces

#!require:orphan_vpcs
orphan_network_interfaces:
  exec.run:
    - path: aws.ec2.network_interface.list
    - kwargs:
        name: null
        filters:
          - Name: vpc-id
            Values:
              {%- for resource in hub.idem.arg_bind.resolve('${exec:orphan_vpcs}') %}
              - {{ resource['resource_id'] }}
              {% endfor -%}

#!require:orphan_network_interfaces
{% for resource in hub.idem.arg_bind.resolve('${exec:orphan_network_interfaces}') %}

# Remove leftover Network Interfaces
detach-network_interface-{{ resource['resource_id'] }}:
  exec.run:
    - path: boto3.client.ec2.detach_network_interface
    - kwargs:
        AttachmentId: {{ resource['attachment_id'] }}
        Force: True

cleanup-network_interface-{{ resource['resource_id'] }}:
  exec.run:
    - path: boto3.client.ec2.delete_network_interface
    - kwargs:
        NetworkInterfaceId: {{ resource['resource_id'] }}
    - require:
        - detach-network_interface-{{ resource['resource_id'] }}
        -
{% endfor %}
