import copy
import json
import time
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_policy(hub, ctx):
    policy_temp_name = "idem-test-policy-" + str(int(time.time()))
    policy_document = {
        "Statement": [
            {"Action": ["ec2:DescribeTags"], "Effect": "Allow", "Resource": "*"}
        ],
        "Version": "2012-10-17",
    }

    description = "Idem IAM policy integration test"
    tags = {"Name": policy_temp_name}
    ret = await hub.states.aws.iam.policy.present(
        ctx,
        name=policy_temp_name,
        policy_document=policy_document,
        description=description,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    resource = ret["new_state"]
    # For policy arn is the identifier used to find an existing object
    resource_id = resource.get("resource_id")
    assert resource_id, "Expecting resource id"
    assert policy_temp_name == resource.get("name")
    assert description == resource.get("description")
    assert tags == resource.get("tags")
    assert json.dumps(policy_document, sort_keys=True) == resource.get(
        "policy_document"
    )

    # Test updating policy document and adding tags
    tags.update(
        {
            f"iam-policy-key-{str(uuid.uuid4())}": f"iam-policy-value-{str(uuid.uuid4())}",
        }
    )
    policy_document = '{"Statement": [{"Action": ["ec2:CreateSubnet"], "Effect": "Allow", "Resource": "*"}], "Version": "2012-10-17"}'
    ret = await hub.states.aws.iam.policy.present(
        ctx,
        name=policy_temp_name,
        resource_id=resource_id,
        policy_document=policy_document,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert tags == resource.get("tags")

    assert (
        ret.get("old_state")["default_version_id"]
        != ret.get("new_state")["default_version_id"]
    )

    # Testing with test flag - start
    # Create policy with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.iam.policy.present(
        test_ctx,
        name=policy_temp_name + "-test",
        policy_document=policy_document,
        description="test-ctx-description",
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert "Would create" in str(ret["comment"]), ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert resource.get("description") == "test-ctx-description"

    # Update tags and change policy on existing policy with test flag
    test_tags = {"Test-Name": policy_temp_name + "test"}
    test_policy_document = {
        "Statement": [
            {"Action": ["ec2:DeleteVpc"], "Effect": "Allow", "Resource": "*"}
        ],
        "Version": "2012-10-17",
    }
    ret = await hub.states.aws.iam.policy.present(
        test_ctx,
        name=policy_temp_name,
        resource_id=resource_id,
        policy_document=test_policy_document,
        description=description,
        tags=test_tags,
    )
    assert ret["result"], ret["comment"]
    assert "Would update" in str(ret["comment"]), ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert test_tags == resource.get("tags")
    assert resource.get("policy_document") == json.dumps(test_policy_document)

    # Delete policy with test flag
    ret = await hub.states.aws.iam.policy.absent(
        test_ctx,
        name=policy_temp_name,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert "Would delete" in str(ret["comment"]), ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    # Testing with test flag - end

    # Test deleting tags
    tags = {"Name": policy_temp_name}
    ret = await hub.states.aws.iam.policy.present(
        ctx,
        name=policy_temp_name,
        resource_id=resource_id,
        policy_document=policy_document,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert tags == resource.get("tags")

    # Verify no change to policy
    assert (
        ret.get("old_state")["default_version_id"]
        == ret.get("new_state")["default_version_id"]
    )

    # Describe IAM policy
    describe_ret = await hub.states.aws.iam.policy.describe(ctx)
    resource_key = f"iam-policy-{resource_id}"
    assert resource_key in describe_ret
    assert "aws.iam.policy.present" in describe_ret.get(resource_key)
    resource = describe_ret[resource_key]["aws.iam.policy.present"]
    described_resource_map = dict(ChainMap(*resource))
    assert policy_temp_name == described_resource_map.get("name")
    assert policy_document == described_resource_map.get("policy_document")
    assert resource_id == described_resource_map.get("resource_id")
    assert tags == described_resource_map.get("tags")

    # Delete IAM policy
    ret = await hub.states.aws.iam.policy.absent(
        ctx, name=policy_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Delete IAM policy again
    ret = await hub.states.aws.iam.policy.absent(
        ctx, name=policy_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
